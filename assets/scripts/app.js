const defaultResult = 0;
let currentResult = defaultResult;
let logEntries = [];

//Obtener info del input de entrada
function getUserNumberInput() {
  return parseInt(userInput.value);
}

//Se genera y escribe un registro de los calculos
function createAndWriteOutput(operator, resultBeforeCalc, calcNumber) {
  const calcDescription = `${resultBeforeCalc} ${operator} ${calcNumber}`;
  outputResult(currentResult, calcDescription); //funcion del archivo vendor.js
}

function writeToLog(
  operationIdentifier,
  prevResult,
  operationNumber,
  newResult
) {
    const logEntry = {
        operation: operationIdentifier,
        prevResult: prevResult,
        number: operationNumber,
        result: newResult,
      };
    
      logEntries.push(logEntry);
      console.log(logEntry);
}

function add() {
  const enterdNumber = getUserNumberInput();
  const initialResult = currentResult;
  //   currentResult = currentResult + enterdNumber;
  currentResult += enterdNumber;
  createAndWriteOutput("+", initialResult, enterdNumber);
  writeToLog('ADD', initialResult, enterdNumber, currentResult);
}

function subtract() {
  const enterdNumber = getUserNumberInput();
  const initialResult = currentResult;
  // currentResult = currentResult - enterdNumber;
  currentResult -= enterdNumber;
  createAndWriteOutput("-", initialResult, enterdNumber);
  writeToLog('SUBTRACT', initialResult, enterdNumber, currentResult);
}

function multiply() {
  const enterdNumber = getUserNumberInput();
  const initialResult = currentResult;
  // currentResult = currentResult * enterdNumber;
  currentResult *= enterdNumber;
  createAndWriteOutput("*", initialResult, enterdNumber);
  writeToLog('MULTIPLY', initialResult, enterdNumber, currentResult);
}

function divide() {
  const enterdNumber = getUserNumberInput();
  const initialResult = currentResult;
  // currentResult = currentResult / enterdNumber;
  currentResult /= enterdNumber;
  createAndWriteOutput("/", initialResult, enterdNumber);
  writeToLog('DIVIDE', initialResult, enterdNumber, currentResult);
}

addBtn.addEventListener("click", add);
subtractBtn.addEventListener("click", subtract);
multiplyBtn.addEventListener("click", multiply);
divideBtn.addEventListener("click", divide);

// currentResult = add(3, 2);
// currentResult = (currentResult + 10) * 3 / 2 - 1;
// let calculationDescription = `(${defaultResult}  + 10) * 3 / 2 - 1`;
