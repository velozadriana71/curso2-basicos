const defaultResult = 0;
let currentResult = defaultResult;
let logEntries = [];

//Obtener info del input de entrada
function getUserNumberInput() {
  return parseInt(userInput.value);
}

//Se genera y escribe un registro de los calculos
function createAndWriteOutput(operator, resultBeforeCalc, calcNumber) {
  const calcDescription = `${resultBeforeCalc} ${operator} ${calcNumber}`;
  outputResult(currentResult, calcDescription); //funcion del archivo vendor.js
}

function writeToLog(
  operationIdentifier,
  prevResult,
  operationNumber,
  newResult
) {
  const logEntry = {
    operation: operationIdentifier,
    prevResult: prevResult,
    number: operationNumber,
    result: newResult,
  };

  logEntries.push(logEntry);
  console.log(logEntry);
}

function calculateResult(calculationType) {
    const enterdNumber = getUserNumberInput();
  if (
    calculationType === "ADD" &&
    calculationType === "SUBTRACT" &&
    calculationType === "MULTIPLY" &&
    calculationType === "DIVIDE" ||
    !enterdNumber
  ) {
    return;
  }
    const initialResult = currentResult;
    let mathOperator;
    if (calculationType == "ADD") {
      currentResult += enterdNumber;
      mathOperator = "+";
    } else if (calculationType === "SUBTRACT") {
      currentResult -= enterdNumber;
      mathOperator = "+";
    } else if (calculationType === "MULTIPLY") {
      currentResult *= enterdNumber;
      mathOperator = "*";
    } else if (calculationType === "DIVIDE") {
      currentResult /= enterdNumber;
      mathOperator = "/";
    }
    createAndWriteOutput(mathOperator, initialResult, enterdNumber);
    writeToLog(calculationType, initialResult, enterdNumber, currentResult);
  }

function add() {
  calculateResult("ADD");
}

function subtract() {
  calculateResult("SUBTRACT");
}

function multiply() {
  calculateResult("MULTIPLY");
}

function divide() {
  calculateResult("DIVIDE");
}

addBtn.addEventListener("click", add);
subtractBtn.addEventListener("click", subtract);
multiplyBtn.addEventListener("click", multiply);
divideBtn.addEventListener("click", divide);
